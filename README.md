# Utkarsh Singhal

## Hi there! 👋

Welcome to my GitLab profile! 🚀

I'm Utkarsh, an analytics engineer by passion with a keen interest in utilizing best software practices to build data products while keeping business goals in mind.

## About Me

- 💻 I'm currently working as an [`Analytics Engineer`](https://handbook.gitlab.com/job-families/finance/analytics-engineer/) at GitLab.
- 📊 I majored in Finance but I love transforming raw data into gold, crafting elegant pipelines, sculpting robust architectures.
- 😎 Outside of work, you'll find me cycling, swimming, reading about geopolitics, occasionally [blogging](https://tradewithpython.com/) and enjoying parties!

## How I Work

- 🔍 **Curious Learner:** I like to ask questions while approaching tasks with a curious mindset.
  
- 🛠️ **Methodical:** I like to take a systematic approach to my work, breaking tasks down into manageable steps to achieve success.
  
- 🤝 **Collaborative:** I value open communication and teamwork to accomplish goals efficiently.

**Work Timings:** I usually work from 10:30 AM to 7:30 PM IST

## Get in Touch
- 📫 Feel free to reach out to me via [Email](mailto:utkarshsinghal06@gmail.com) or connect with me on [LinkedIn](https://www.linkedin.com/in/utkarsh-singhal-/).
- 💬 Feel free to reach out for a friendly chat or to discuss ideas!

Thanks for visiting my profile! 😊
